chat_models = mongoose.model('chat', {
  author: String,
  messages: String,
  time: {
    type: Date,
    default: Date.now()
  }
});
